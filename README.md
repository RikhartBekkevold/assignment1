# Author name with student ID number. #


Author: Rikhart Bekkevold

Studnr: 091036


# Comment on Attributions #

Most of the project code is basically tutorials I just intended as a base. Because of so many problems I never really got to the next step of changing it/creating new, only
a little. That's basically the reasoning behind the use of the attributed code.


# Description of the Application. #

The application contains several activities. A main activity, a map activity, an activity never shown to user, which performs an operation and sends information back, and an activity which shows the last coordinates of the device. (PS. The layout etc, in the app, doesnt make much practical sense.)

On the main activity, at the bottom, is a text field which the user can change. This can be changed to anything. If the user restarts/reboots, the next time the app is launched the text will still be the same. It will be remembered between app uses as a ‘preference’.

The button on the middle of the screen starts another activity, sending a string with to it, this activity (never visually seen) then appends information to the string and sends it back.

Clicking the top most button ‘see your location coordinates’, jumps to another activity and there displays the devices last location. Obs! it is sometimes slow to show and wont show immidiately, and sometimes prompts you to enable location for your device. Try going to the map and back. But most of all:  Use a phone/usb connected device if possible. All of the problems with GPS seems to be related to the emulators.
Clicking the button, “go to google map”, on this page sends you to a google map view.


 

# Discussion of the use of stored data when restarting from shutdown. #



In the field in the main activity, the app remembers, in a saved preference file, the last typed in word by the user.
I also wanted to save more, perhaps in a db, based on location history or the like.
This storage was made primarily as an example of what it can be used for, without much practical use in this app specifically.


# Discussion of the difference between native Apps and web Apps, with reference to the application developed. #


The functionality of both native apps and a web app can be overlapping. It’s not a clear border and some of the functionality is shared by both. The difference is that a native app is one that is installed directly onto the smart phone and can work, in most cases, with no internet connectivity depending on a few factors. A web app works via web browser on the smartphone but requires a form of connectivity.
The benefits of the native app is that it can work independently of the web, but most are pulling information or function from the web in some way. There is an aspect of “behind the scenes” in the native app that is presenting web content within the app itself without a browser.  The native app can work much faster by harnessing the power of the processor. This app is faster since it is native, although it would probably have little impact in this instance. Native apps can also access specific hardware like GPS. In this app GPS was used. The usage of this relied on the implementation of a native app. Without this beeing a native app it would be very hard to take advantage of this hardware.

The web app has the fortune of being used on various devices with the only requirement being a web browser and an internet connection. The web app only needs to be written for cross browser compatibility. The dilemma is that in contrast to the native app, it requires internet access and its operation speeds are dependent on the quality of cell signal or the speed of the wi-fi broadband you are connected to. This also alludes to the fact that you have to be in range of either connection. One other issue is you won’t have access to internal hardware such as GPS and other connectivity. This accessibility was a huge advantage for this app.
The difference of native versus web often comes down to function versus the ease of porting to various devices. The app developed can be used anywhere, anytime. The consideration that had to be taken in regards to various devices where a pain, solved mostly by only targeting one device/apk.

In this case, where distribution is not needed, a web app looses some of it’s appeal. 

For every mobile platform there is a different programming language used. For example in the development of this app java was needed. A programming language I have never used and then needed to learn alongside development. Web apps gives the potential to choose more based on preference/standards and not platform. 


# Explanation of how the app could be extended. #



The app could be extended by letting more of the information left behind by the user be stored as preferences so that they would be there on return. Currently only one variable is stored for later return. Of course this would be more relevant if there where more of interest to store which there currently isn’t. The app could potentionally also use sqlite db to store information about last time user visited different locations. The coordinates could also be visually represented on the google map with the marker and camera location.

By getting more information from the GPS/user more things can be done with the map, for example the app could log locations the user has been too. Keep a sort of history. Or perhaps the app could pick out the shortest path between a user choosen location and his/hers current. The library is extensive and lots of possiblities is plausible.

Use of GPS is the fundament of so many apps. For example workout apps or health apps. This app could move in such a direction. By implementing other sensors like motion sensors for example, step detector  or the like, the potential for an health app or similar is great.  


# (Include in the report) what was hard (or too hard), and what turned out easy, but you initially thought being hard. #


Perhaps the usage of Java turned out a bit easier then initially believed. If you know some programming languages it isnt that hard to understand others.
Googling problems encountered, turned out harder then expected. I guess it was hard to formulate the questions accurately enough. Some problems where probably to specific aswell. 

In terms of actually using the libraries and android studio. The code in form of using the ui elements where pretty clear and intuitive. 

Everything turned out harder then i thought it would be, to be honest. Overall it was very frustrating because things that worked for others didn’t always for me.
I had so many problems I never expected i would. It was mostly related to quirky/wierd things aswell as some ambigous/unclear tutorials. The use of the emulator turned out to be problematic aswell. For example the emulator would not return the last location from the GPS and this caused confusion. Like in this example, I often confused thinking I didn’t understand the code, with the actual problem. Pin-pointing what was actually wrong/causing the problem was very difficult. As expected perhaps, just even more so. 

I felt like i never got a chance to focus on the code with so many random problems in the way. This was the biggest problem/hardest part.