package com.example.rikhartsorrow.myapplication;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;


public class SavedActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity__saved);

        // Gets the intent that started the activity
        Intent intent = getIntent();

        // Extract the string extra sent with the intent/request
        String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);

        // Add before this message another message
        String resultMessage = message + "This line is appended from the other activity.";


        // Send back parsed message
        Intent result = new Intent("com.example.RESULT_ACTION", Uri.parse(resultMessage));
        setResult(SavedActivity.RESULT_OK, result);

        // Close down activity
        finish();
    }

}
