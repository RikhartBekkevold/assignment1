package com.example.rikhartsorrow.myapplication;


import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

    public final static String EXTRA_MESSAGE = "com.example.myfirstapp.MESSAGE";
    //request code. Must be higher then 0
    static final int PICK_CONTACT_REQUEST = 1;



    /** Called upon activity creation (first time or resumed). Adds values to db each time... **/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        /** Snippet taken from:
            http://stackoverflow.com/questions/3624280/how-to-use-sharedpreferences-in-android-to-store-fetch-and-edit-values
            Did exactly what I was trying to do in a really simple way. Code inside if statement my own.
         */
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String name = preferences.getString("Name", "");
        if(!name.equalsIgnoreCase(""))
        {
            EditText titleText = (EditText) findViewById(R.id.titleText);
            titleText.setText(name);
        }
    }


    @Override /** Called when returning from SavedActivity with resulting string **/
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request it is that we're responding to
        if (requestCode == PICK_CONTACT_REQUEST) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {

                // Extract result
                String result = data.getData().toString();

                // Display
                TextView feedback = (TextView) findViewById(R.id.feedback);
                feedback.setTextSize(15);
                feedback.setText(result);

            }
        }
    }

    /** Called when the user clicks the Send button **/
    public void sendMessage(View view) {

        // Sends message from the edit view to the next activity
        Intent intent = new Intent(this, DisplayMessageActivity.class);
        startActivity(intent);
    }


    /** Jumps to new activity (saved activity) with a message and request for result.
       (you don't pick anything on this activity(auto operation), so you don't get to
       see the activity) **/
    public void pickContact(View view) {
        Intent intent = new Intent(this, SavedActivity.class);
        intent.putExtra(EXTRA_MESSAGE, "This line is sent from this activity. ");
        startActivityForResult(intent, PICK_CONTACT_REQUEST);
    }

    /** When called inserts the values from the text views into the DB **/
    public void addToDB(View view){

        EditText titleText = (EditText) findViewById(R.id.titleText);
        String prefText = titleText.getText().toString();

        /** Code snippet taken from:
            http://stackoverflow.com/questions/3624280/how-to-use-sharedpreferences-in-android-to-store-fetch-and-edit-values
            Did exactly what I was trying to do in a really simple way.
         */
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("Name", prefText);
        editor.apply();

    }


}
